# Registro das decisões arquiteturais do projeto Padrão Digital de Governo - Design System

Repositório onde versionamos as decisões arquiteturais do projeto que tragam impactos ao projeto, para que possam ser consultadas sempre que necessários. 

Para entender um pouco mais sobre esse processo, sugerimos a leitura das seguintes documentações:

- [Architecture decision record - ADR](https://github.com/joelparkerhenderson/architecture-decision-record)
- [O que é architecture decision record - ADR](https://pt.linkedin.com/pulse/o-que-é-architecture-decision-record-adr-e-como-se-jonas-frança)

## Passos para criar uma ADR

1. Criar uma branch com o nome docs/titulo
1. Criar o documento seguindo o template abaixo
1. Criar o MR e incluir as Tags dos perfis impactados e fazer as associações relacionadas a dependencias [exemplo](https://docs.gitlab.com/ee/user/project/merge_requests/dependencies.html)
1. Incluir as Tags dos perfis impactados dev, design, devops, designops
1. Enquanto a fase do ADR estiver em fase de Decisão do MR deve estar como Draft
1. Após decisão do time, atualizar o documento e submeter um MR
1. Teremos que ter pelo menos x aprovadores para considerar o documento aprovado.

## Modelo de Template

# <titulo> Título da ADR
O título deve informar o propósito e ser curto e objetivo

Date: <date>
Incluir a data da tomada de decisão

## Status
- **Em proposição:** Foi criado o documento e criado o MR, porém o mesmo ainda consta como Draft
- **Preparada:** A decisão está clara o suficiente para que todos do time entenda, e foi retirado o Status de Draft do MR
- **Rejeitada:** Não foi aceita pelo time, optamos por descartar o item(Como manter o historico?)
- **Aprovada:** Foi aprovada pelo time e aceito o MR.
- **Depreciada:** Deletar o arquivo e submeter um MR fazendo as devidas associações


## Contexto

Explicação da ADR de forma a trazer clareza sobre a decisão a ser tomada, importante que seja bem direto e simples, facilitando o entendimento.   
Listar as alternativas dos possíveis caminhos/solução avaliadas

## Decisão

Incluir a conclusão realizada diante das forças e considerações relevantes, ao preencher solicite a validação de um membro para avaliar se o texto informado ficou claro e completo.

## Justificativa

Descreva aqui a justificativa para a decisão de projeto. Indique também a justificativa para alternativas significativas que foram rejeitadas. Esta seção pode também listar premissas, restrições, requisitos e resultados de avaliações e experimentos.


## Consequências

Após a decisão tomada, incluir as consequências da tomada de decisão, incluindo as positivas e negativas quando houver, tudo que possa afetar o time e o projeto deve estar registrado.
